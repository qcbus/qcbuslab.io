import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'qcbus';
  mine = getTime();
  lastUpdated = 'Last updated: ' + new Date().toString();
}

function getTime() {
  const xhr = new XMLHttpRequest(),
    method = 'GET',
    url = 'https://cors.io/?https://queenscollegeshuttle.com/Stop/3143185/Arrivals';
  xhr.open(method, url, true);
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4 && xhr.status === 200) {
      console.log(xhr.responseText);
      if (xhr.responseText == '[]') {
        console.log('we got here');
        document.getElementById('bus-data').innerText = 'probably no bus today?'
      } else {
        document.getElementById('First-bus').innerText =
          JSON.parse(
            xhr.responseText)[0].Arrivals[0].ArriveTime;
        document.getElementById('Second-bus').innerText =
          JSON.parse(
            xhr.responseText)[0].Arrivals[1].ArriveTime;
        document.getElementById('Third-bus').innerText =
          JSON.parse(
            xhr.responseText)[0].Arrivals[2].ArriveTime;
      }
    }
  };
  xhr.send();
  return '';
}

const a = function () {
  setInterval(function () {
    document.getElementById('currentTime').innerText = 'Current:' + new Date().toString();
  }, 1000);
};
a();
